#!/bin/sh
locale=$(locale | grep TIME | cut -c10-11)
if [ "uk" = "$locale" ]; then
    echo "UA locale"
    conky -c ~/.config/conky/.conkyrc1_ua -d &
    #conky -c ~/.config/conky/.conkyrc2_ua -d &
elif [ "ru" = "$locale" ]; then
    echo "RU locale"
    conky -c ~/.config/conky/.conkyrc1_ru -d &
    #conky -c ~/.config/conky/.conkyrc2_ru -d &
else
    echo "EN locale"
    conky -c ~/.config/conky/.conkyrc1_en -d &
    #conky -c ~/.config/conky/.conkyrc2_en -d &
fi
