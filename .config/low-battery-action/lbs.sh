#!/bin/bash

###########################################################################
#
# Usage: system-low-battery
#
# Checks if the battery level is low. If “low_threshold” is exceeded
# a system notification is displayed, if “critical_threshold” is exceeded
# a popup window is displayed as well. If “OK” is pressed, the system
# shuts down after “timeout” seconds. If “Cancel” is pressed the script
# does nothing.
#
# This script is supposed to be called from a cron job.
#
###########################################################################

# This is required because the script is invoked by cron. Dbus information
# is stored in a file by the following script when a user logs in. Connect
# it to your autostart mechanism of choice.
#
# #!/bin/sh
# touch $HOME/.dbus/Xdbus
# chmod 600 $HOME/.dbus/Xdbus
# env | grep DBUS_SESSION_BUS_ADDRESS > $HOME/.dbus/Xdbus
# echo 'export DBUS_SESSION_BUS_ADDRESS' >> $HOME/.dbus/Xdbus
# exit 0
#
if [ -r ~/.dbus/Xdbus ]; then
  . ~/.dbus/Xdbus
fi

low_threshold=30
critical_threshold=26
timeout=60
#shutdown_cmd='/usr/sbin/pm-hibernate'
shutdown_cmd='/usr/sbin/poweroff'

level=$(cat /sys/class/power_supply/BAT1/capacity)
state=$(cat /sys/class/power_supply/BAT1/status)

if [ x"$state" != x'Discharging' ]; then
  exit 0
fi

do_shutdown() {
  sleep $timeout && kill $zenity_pid 2>/dev/null

  if [ x"$state" != x'Discharging' ]; then
    echo "Exit 0 from do_shutdown"
    exit 0
  else
    echo "shutdown now"
#    systemctl suspend
    $shutdown_cmd
  fi
}

if [[ "$level" -gt $critical_threshold && "$level" -lt $low_threshold ]]; then
#if [ "$level" -lt $low_threshold ]; then
#  notify-send "Battery level is low: $level%"
#echo "Воно взагалі сюди заходить?"
    DISPLAY=:0 zenity --info --text="Рівень баратеї $level%. Будь ласка, підключіть зарядний пристрій."
fi
if [ "$level" -lt $critical_threshold ]; then

  DISPLAY=:0 zenity --info \
    --text "Уровень заряда батареї критически низок. $level%.\n\n Компьютер выключится через одну минуту. Сохраните свою работу" &
  zenity_pid=$!
  do_shutdown &
  shutdown_pid=$!

  trap 'kill $shutdown_pid' 1

  if ! wait $zenity_pid; then
    kill $shutdown_pid 2>/dev/null
  fi
fi

exit 0
