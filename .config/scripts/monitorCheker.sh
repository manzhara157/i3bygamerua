#!/bin/bash

firstMon=$(xrandr | grep ' connected' | head -n1 | cut -d" " -f1)
secondMon=$(xrandr | grep ' connected' | tail -n1 | cut -d" " -f1)
#echo $firstMon
#echo $secondMon

if [ $firstMon != $secondMon ]
then 
    connected=$secondMon
else
    connected=""
fi
resolution=$(xrandr | grep -e '*+' -e ' +' | cut -c4-13)
resolutionOne=$(echo $resolution | cut -c1-9)
resolutionOneW=$(echo $resolutionOne | cut -c1-4)
resolutionTwo=$(echo $resolution | cut -c10-19)

echo $connected
#echo $resolutionOne
#echo $resolutionTwo
#echo $resolutionOneW

eDPpHDMIoff () {
    xrandr --output $firstMon --mode $resolutionOne --primary
    xrandr --output $secondMon --off
    #pkill conky
    #~/.config/conky/conkyLauncher.sh
}

eDPpHDMIon () {
    x0="x0"
    resW="$resolutionOneW$x0"
    xrandr --output $firstMon --mode $resolutionOne --primary
    xrandr --output $secondMon --mode $resolutionTwo --pos $resW
}
HDMIpeDPon () {
    x0="x0"
    resW="$resolutionOneW$x0"
    xrandr --output $firstMon --mode $resolutionOne
    xrandr --output $secondMon --mode $resolutionTwo --primary --pos $resW
}
HDMIpeDPoff () {
    x0="x0"
    resW="$resolutionOneW$x0"
    xrandr --output $firstMon --off
    xrandr --output $secondMon --mode $resolutionTwo --primary --pos $resW
    # Hardcod not good =(
    # conkyOn=$(xrandr | grep -o "$res")
    # if ( -z )
    #     then
        #pkill conky
        #~/.config/conky/conkyLauncher.sh
    # fi
}

if [ -z "$connected" ];
then
    echo 'Monitor is not connected 1'
    if [ $firstMon != $secondMon ]
    then
        xrandr --output $secondMon --off
    fi
    xrandr --output $firstMon --mode $resolutionOne --primary
else
    res="$firstMon connected "
    res+="primary" 
    displayed=$(xrandr | grep -o "$res")
    echo $displayed
    if [ -z "$displayed" ];
    then
        echo 'HDMI primary'
        res="$firstMon connected "
        res+=$resolutionOneW
        displayedeDP=$(xrandr | grep -o "$res")
        echo displayd eDP?
        echo $displayedeDP
        if [ -n "$displayedeDP" ];
        then
            echo 'Monitor HDMI primary, eDP on'
            HDMIpeDPoff
        else
            echo "Monitor HDMI primary, $firstMon off"
            eDPpHDMIoff
        fi
    else
        echo "eDP Prime"
        res="$secondMon connected "
        res+=$resolutionTwo 
        displayedHDMI=$(xrandr | grep -o "$res")
        if [ -z "$displayedHDMI" ];
        then
            echo "$firstMon prime, $secondMon off"
            eDPpHDMIon
        else
            echo "$firstMon prime, $secondMon on"
            HDMIpeDPon
        fi
    fi
fi
