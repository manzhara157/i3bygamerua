#!/bin/bash
#
## Автор скрипту Манжара Владислав. Скрипт створений для особистого користування. 
# Я дозволяю його копіювати, поширювати та модифікувати. 
# Для його роботи вам знадобиться dmenu


bssid=$(nmcli -f SSID,BSSID,SIGNAL,RATE,BARS,SECURITY dev wifi list | sed -n '1!p' | dmenu -p "Виберіть WiFi: " -l 20 | cut -d' ' -f1)
pass=$(echo "" | dmenu -p "Введіть пароль: ")
if [[ -z $pass ]];
then
	echo "Empty"
	nmcli device wifi connect $bssid password ""
else
	echo "Not Empty"
	nmcli device wifi connect $bssid password $pass
fi
