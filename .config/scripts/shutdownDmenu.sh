#!/bin/bash
#
mainMenu=" Shutdown
 Reboot
 Sleep
 Lock
 Cancel
"
# bindsym $mod+x exec dmenu_run  
choos=$(echo -e "$mainMenu" | dmenu -c -nb '#270b09' -sf '#c9c2c1' -sb '#b02967' -nf '#c9c2c1' -fn 'e-Ukraine 14' -l 5 -p "Choose: ")

if [[ $choos == " Shutdown" ]]
then
	systemctl poweroff
fi

if [[ $choos == " Reboot" ]] 
then
	systemctl reboot
fi

if [[ $choos == " Sleep" ]]
then
	systemctl suspend
fi

if [[ $choos == " Lock" ]]
then
	loginctl terminate-session ${XDG_SESSION_ID-}
fi

if [[ $choos == " Cancel" ]]
then
	echo "Cancel"
fi

# if [ $choos == "Logout" ] then
# 	systemctrl poweroff
# fi

