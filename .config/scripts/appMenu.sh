#!/bin/bash
#rofi -modi drun -show drun -display-drun "Apps : " -line-padding 4 -columns 2 -padding 30 -hide-scrollbar -show-icons -drun-icon-theme "Arc-X-D" -font "Kyiv Region Regular 12" -location 1 -yoffset 37 -xoffset 10 -width 49

	ret=$(ps aux | grep wofi | wc -l)
 	if [ "$ret" -lt 2 ]
 then {
 	echo "Running Wofi" #output text
         sleep 1  #delay
	wofi -d -S drun -xoffset 10 -yoffset 37 -I -w 2
 	exit 1
 }
 else 
 {
 	echo "EXIT. Wofi already running!"
 	exit 1
 }
 fi;

