#!/bin/bash
#
# Автор скрипту Манжара Владислав. Скрипт створений для особистого користування. 
# Я дозволяю його копіювати, поширювати та модифікувати. 
# Для його роботи вам знадобиться notify-send.sh від vlevit. https://github.com/vlevit/notify-send.sh
# Скрипти можна знайти на моємо gitlab репозиторії. https://gitlab.com/manzhara157/i3bygamerua
if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "" ]; then
	echo "Usage $0 [up|down|mute]"
	exit 0
fi

if [ "$1" == "up" ]; then
	vol=`amixer set Master 5%+ | grep % | awk '{print $5}' | cut -d "[" -f 2- | cut -d "%" -f 1 | tail -n 1`
fi

if [ "$1" == "down" ]; then
	vol=`amixer set Master 5%- | grep % | awk '{print $5}' | cut -d "[" -f 2- | cut -d "%" -f 1 | tail -n 1`
fi
if [ "$1" == "mute" ]; then
	amixer -q set Master toggle
	vol=`amixer get Master | grep % | awk '{print $5}' | cut -d "[" -f 2- | cut -d "%" -f 1 | tail -n 1`
	echo "toggle"
fi

# Uncomment line below, if you do not use Dunst
# dilnyk=10
# notify-send "Volume" "Volume up to $vol"
# vol10=$(($vol/$dilnyk))
# echo $vol
# echo "$vol10"
# simbol1="/"
# simbol2="-"
# volBar=$simbol1
# for (( i=0; i < $vol10; i++ ))
# do
# 	volBar+=$simbol1
# done
# for (( i=0; i < $dilnyk-$vol10; i++ ))
# do
# 	volBar+=$simbol2
# done

# Without Dunst
# ~/.config/notify-send/notify-send.sh --replace=2 --print-id "Volume: $vol" "$volBar"

# With Dunst
# ~/.config/notify-send/notify-send.sh -a center --replace=2 --print-id "Volume" "Volume: $vol" -h int:value:$vol
isMuted=$(amixer get Master | grep off)
if [[ -z $isMuted ]]; then
	if [ $vol -gt 66 ]; then
		~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/sound_hige.png --replace=2 --print-id "$vol" -h int:value:$vol
	echo "66"
	else
		if [ $vol -gt 33 ]; then
			~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/sound_medium.png --replace=2 --print-id "$vol" -h int:value:$vol
		echo "33"
		else
			~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/sound_low.png --replace=2 --print-id "$vol" -h int:value:$vol
		echo "less"
		fi
	fi
else
	~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/sound_mute.png --replace=2 --print-id "Mute" -h int:value:0
	echo "mutted"
fi



