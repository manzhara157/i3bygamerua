#!/bin/bash

# "/home/gamerua/.local/program/picom/build/src/picom --experimental-backend --config /home/gamerua/.config/p.conf", 
ret=`ps aux | grep battaryChecker | wc -l`
pid=`ps aux | grep battaryChecker | awk '{print $2}' | head -n 1`
if [ "$ret" -lt 2 ]
then 
	echo "BattaryCheker scip"
else
	kill $pid
fi

ret=`ps aux | grep conky | wc -l`
echo $ret
pid=`ps aux | grep conky | awk '{print $2}' | head -n 1`
if [ "$ret" -lt 2 ]
then 
	echo "conky scip"
else
	kill $pid
fi

sh /home/gamerua/.config/conky/conkyLauncher.sh &
sh /home/gamerua/.config/scripts/battaryChecker.sh &
