#!/bin/bash
#
dunstify "YouScript" "Search resolution"
url=$(xsel -ob)
isUrl=$(echo $url | grep http)
echo $isUrl
menu="Download
Download Audio
Play
No Video
"
if [[ -n $isUrl ]]; then
	audio=$(yt-dlp -F $url | grep audio | head -n 1 | awk '{print $1}')
	video=$(yt-dlp -F $url | grep video | dmenu -l 10 -nb '#270b09' -nf '#c9c2c1' -sb '#b02967' -sf '#c9c2c1' | awk '{print $1}')
	echo $video
	echo $audio
	whatDo=$(echo -e "$menu" | dmenu -nb '#270b09' -nf '#c9c2c1' -sb '#b02967' -sf '#c9c2c1' -fn 'e-Ukraine 17' -p "Whot to do?")
	if [[ "$whatDo" == "Download" ]]; then
		notify-send "YouScript" "Start Downloads"
		yt-dlp -P "~/Downloads/" -f $video+$audio $url | awk '{print $2}' 
	fi
	
	if [[ "$whatDo" == "Download Audio" ]]; then
		notify-send "YouScript" "Start Downloads"
		yt-dlp -P "~/Downloads/" -f 139 $url | awk '{print $2}' 
	fi

	if [[ "$whatDo" == "Play" ]]; then
		echo "Play"
		mpv --ytdl-format=$video+$audio $url
	fi
	
	if [[ "$whatDo" == "No Video" ]]; then
		echo "Play"
		kitty mpv --no-video $url
	fi
fi
notify-send "YouScript" "Done!"

