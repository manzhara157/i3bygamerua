#!/bin/sh


if zenity --calendar \
--title="Select a Date" \
--text="Click on a date to select that date." 
  then echo $?
  else echo "No date selected"
fi
