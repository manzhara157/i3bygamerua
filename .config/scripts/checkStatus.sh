#!/bin/bash

tempCPU=$(sensors | grep "Core 0" | awk '{print $3}')
#loadCPU_US=$(vmstat | tail -n1 | awk '{print $13}')
#loadCPU_SY=$(vmstat | tail -n1 | awk '{print $14}')
loadCPU=$(top -bn 2 -d 0.01 | grep 'Проц' | tail -n 1 | gawk '{print $2+$4+$6}')
ram=$(free -th | grep Пам | awk '{print $3}')
home=$(df -h | grep home | awk '{print $4}')
echo $tempCPU
echo $ram
echo $home

homePers=$(df -h | grep home | awk '{print $5}' | rev | cut -c 2- | rev)
root=$(df -h | grep sda3 | awk '{print $4}')
rootPers=$(df -h | grep sda3 | awk '{print $5}' | rev | cut -c 2- | rev)
homePers2=$(($homePers/10))
rootPers2=$(($rootPers/10))
symbol1="H"
symbol2="-"
homeBar="|"
rootBar="|"
for ((i=0 ; i <= $homePers2 ; i++))
do
	homeBar+=$symbol1
done
for ((i=0 ; i <= 10-$homePers2 ; i++))
do
	homeBar+=$symbol2
done
for ((i=0 ; i <= $rootPers2 ; i++))
do
	rootBar+=$symbol1
done
for ((i=0 ; i <= 10-$rootPers2 ; i++))
do
	rootBar+=$symbol2
done
homeBar+="|"
rootBar+="|"
echo CPU $rootBar

notify-send "Стан Системи" $"\n \
CPU t°			     ${tempCPU}\n \
CPU load		         ${loadCPU}%\n \
RAM			       ${ram}\n \
Free Space /     ${root} $rootBar\n \
Free Space /home ${home} $homeBar \
	"
