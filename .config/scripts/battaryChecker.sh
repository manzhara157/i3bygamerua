#!/bin/bash
#
#Battery Cheker
# Автор скрипту Манжара Владислав. Скрипт створений для особистого користування. 
# Я дозволяю його копіювати, поширювати та модифікувати. 
# Для його роботи вам знадобиться notify-send.sh від vlevit. https://github.com/vlevit/notify-send.sh


acNew=$(acpi --ac-adapter | awk '{print $3}')
gov=$(cpufreq-info | grep decide | awk '{print $3}' | head -n 1)
acOld="on-line"
govOld="\"ondemand\""
isExit=1
bc=$(acpi --battery | awk '{print $4}' | cut -d '%' -f1)

while [ $isExit = 1 ] 
do 
	 bc=$(acpi --battery | awk '{print $4}' | cut -d '%' -f1)
	 if [[ $bc < 30 && $bc != 100 ]]
	 # if [[ $bc < 30 ]]
	 then
	 	# notify-send "Battery" "Battery charge is $bc"
		~/.config/notify-send/notify-send.sh --replace=3 --print-id "Battery" "Battery charge is $bc"

	 fi

	acNew=$(acpi --ac-adapter | awk '{print $3}')
	if [ $acOld = $acNew ]; then
			echo "All good"
			echo $bc
			echo $acOld
			echo $acNew
		else
			acOld=$(acpi --ac-adapter | awk '{print $3}')
			if [ "$gov" = $govOld ]; then 
				cpufreq-set -c 0 -g powersave
				gov=$(cpufreq-info | grep decide | awk '{print $3}' | head -n 1)
				echo "Switch to $gov"
				notify-send "Battery" "Battery is $acNew"
			else
				cpufreq-set -c 0 -g ondemand
				gov=$(cpufreq-info | grep decide | awk '{print $3}' | head -n 1)
				echo "Switch to $gov"
				notify-send "Battery" "Battery is $acNew"
			fi
			# echo $acOld
			# echo $acNew
		fi
	sleep 5
done
