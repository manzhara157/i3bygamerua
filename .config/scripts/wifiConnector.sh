#!/bin/bash

bssid=$(nmcli -f SSID,BSSID,SIGNAL,RATE,BARS,SECURITY dev wifi list | sed -n '1!p' | dmenu -c -nb '#270b09' -sf '#c9c2c1' -sb '#b02967' -nf '#c9c2c1' -fn 'e-Ukraine 15' -p "Обери WiFi: " -l 20 | awk '{print $2}')
pass=$( echo "" | dmenu -P -c -nb '#270b09' -sf '#c9c2c1' -sb '#b02967' -nf '#c9c2c1' -fn 'e-Ukraine 15' -p "Введіть пароль: ")
echo $bssid

if [[ -n $pass ]]
then
	echo "pass"
	text=$(nmcli device wifi connect $bssid password $pass)
	dunstify "Interner" "$text"
else
	echo "no pass"
	text=$(nmcli device wifi connect $bssid)
	dunstify "Interner" "$text"
fi


