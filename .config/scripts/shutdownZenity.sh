#!/bin/bash

rc=1 # OK button return code =0 , all others =1
ans=$(zenity --info --title 'Викнути?' --window-icon=/home/gamerua/.local/share/icons/Newaita-dark/actions/24/system-shutdown.svg\
    --text 'Хочете завершити роботу?' \
    --ok-label Скасувати \
    --extra-button Вимкнути \
    --extra-button Перезавантажити \
    --extra-button Сон \
    --extra-button Вийти \
     )
rc=$?
echo $ans
if [[ $ans = "Вимкнути" ]]
then
      echo "Вимкнути"
      systemctl poweroff
elif [[ $ans = "Перезавантажити" ]]
then
      echo "Stopping the Rover"
      systemctl reboot
elif [[ $ans = "Сон" ]]
then
      echo "Сон"
      #systemctl hibernate
      systemctl suspend
elif [[ $ans = "Вийти" ]]
then
      echo "Вийти"
      #systemctl 
	  loginctl terminate-session ${XDG_SESSION_ID-}
fi
