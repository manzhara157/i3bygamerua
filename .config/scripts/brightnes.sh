#!/bin/bash

# Автор скрипту Манжара Владислав. Скрипт створений для особистого користування. 
# Я дозволяю його копіювати, поширювати та модифікувати. 
# Для його роботи вам знадобиться notify-send.sh від vlevit. https://github.com/vlevit/notify-send.sh
# Скрипти можна знайти на моємо gitlab репозиторії. https://gitlab.com/manzhara157/i3bygamerua

if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "" ]; then
	echo "Usage $0 [up|down]"
	exit 0
fi
if [ "$1" == "up" ]; then
	br=$(xbacklight -get | cut -d "." -f 1)
	br=$((br+5))
	~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/brightnes_medium.png --replace=2 --print-id "Brightnes" -h int:value:$br
	xbacklight -inc 5
fi

if [ "$1" == "down" ]; then
	br=$(xbacklight -get | cut -d "." -f 1)
	br=$((br-5))
	~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.config/scripts/icons/brightnes_medium.png --replace=2 --print-id "Brightnes" -h int:value:$br
	xbacklight -dec 5
fi
