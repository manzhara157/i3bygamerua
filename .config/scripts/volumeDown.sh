#!/bin/bash
#
# Автор скрипту Манжара Владислав. Скрипт створений для особистого користування. 
# Я дозволяю його копіювати, поширювати та модифікувати. 
# Для його роботи вам знадобиться notify-send.sh від vlevit. https://github.com/vlevit/notify-send.sh
# В заленості від вашої звукової карти для правильної роботи скрипту треба змінити значення в секції awk, у змунній vol.
# $4 або $5 найбільш ймовірні значення.	Це тут	--V

dilnyk=10
msgTag="volume"
vol=`amixer set Master 5%- | grep % | awk '{print $5}' | cut -d "[" -f 2- | cut -d "%" -f 1 | tail -n 1`
vol10=$(($vol/$dilnyk))

# Uncomment line below, if you do not use Dunst
# echo $vol10
# simbol1="/"
# simbol2="-"
# volBar=$simbol1
# for ((i=0; i < $vol10; i++))
# do
# 	volBar+=$simbol1
# done
# for ((i=0; i < $dilnyk-$vol10; i++))
# do
# 	volBar+=$simbol2
# done

# Without Dunst
# ~/.config/notify-send/notify-send.sh --replace=2 --print-id "Volume: $vol" "$volBar"

# With Dunst
~/.config/notify-send/notify-send.sh -a center -u low --icon=$HOME/.local/share/icons/sound_hige.png --replace=2 --print-id "$vol" -h int:value:$vol
