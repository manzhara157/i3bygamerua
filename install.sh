#!/bin/bash

#install i3-gaps
#echo "#i3-gaps repo from ubuntu" >> /etc/apt/sources.list.d/i3-gaps.list
#echo "deb http://ppa.launchpad.net/kgilmer/speed-ricer/ubuntu focal main" >> /etc/apt/sources.list.d/i3-gaps.list
#echo "deb-src http://ppa.launchpad.net/kgilmer/speed-ricer/ubuntu focal main" >> /etc/apt/sources.list.d/i3-gaps.list
#apt update 2>&1 1>/dev/null | sed -ne 's/.*NO_PUBKEY //p' | while read key; do if ! [[ ${keys[*]} =~ "$key" ]]; then apt-key adv --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys "$key"; keys+=("$key"); fi; done
#apt update
#
#install all pakage
apt install xserver-xorg bash-completion sudo mc htop lightdm zenity network-manager-gnome viewnior i3 i3-gaps-wm conky polybar nitrogen picom kitty firefox-esr thunar rofi flameshot

#install config of i3
cd
wget https://gitlab.com/manzhara157/i3bygamerua/-/archive/test/i3bygamerua-test.zip
unzip i3bygamerua-test.zip
cd i3bygamerua-test
cp -r .config/ ~/
cp -r .fonts/ ~/
cp -r .local/ ~/
cd ..
rm -r i3bygamerua-test
rm i3bygamerua-test.zip
