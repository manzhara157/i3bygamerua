# i3byGamerua

Цей репозитарій містить конфіги до різних wm, якими Я користувавсь. В гілці master зберігаються конфіги Awesome. Якщо вас цікавлять i3wm чи sway, то оберіть відповідні гілку.


Конфіг до i3 можна знайти тут: ![посилання](https://gitlab.com/manzhara157/i3bygamerua/-/tree/i3/.config)
Конфіг до sway можна знайти тут: ![посилання](https://gitlab.com/manzhara157/i3bygamerua/-/tree/sway/.config)


Я просто зберіг тут налаштування конфігів до Awesome. Якщо захочете собі такий стіл, то можна просто скопіювати вміст репозитарія. Також в кінці буде описаний спосіб для "лінивих". Дещо треба буде налаштовувати вручну.

![i3](.local/share/backgrounds/screenshot.png)

## Сайти
```
Дрібні налаштування, в тому числі регулювання яскравості
https://debianforum.ru/index.php?topic=14706.0
morc_menu
https://github.com/Boruch-Baum/morc_menu
Налаштування розкладки
https://habr.com/ru/post/150100/
https://igancev.ru/2020-01-05-installing-and-configuring-i3wm-on-arch-linux
Display Manager
https://wiki.debian.org/DisplayManager
TouchPad
https://wiki.debian.org/SynapticsTouchpad
Low Battery
https://unix.stackexchange.com/questions/84437/how-do-i-make-my-laptop-sleep-when-it-reaches-some-low-battery-threshold
NO_KEYPUB
https://www.linuxuprising.com/2019/06/fix-missing-gpg-key-apt-repository.html
gnome-software
https://c-nergy.be/blog/?p=14058
```
## Программы
Мінімальний пакет
- lightdm (Dsiplay Manager)
- bash-completion (Автодописування термінальних команд)
- sudo (Розширення прав користувача)
- picom (cтавити з github) (Композитний менеджер)
- kitty (Емулятор терміналу)
- firefox (Браузер)

За бажанням. 
- zsh (функціональніший командний рядок)
- neovim (Vim-подібний термінальний текстовий редактор)
- ranger і/або thunar і/або mc (Файловий менеджер)
- htop (термінальний менеджер процесів)
- conky (програма для виведення інформації)
- network-manager-gnome (програма для керування мережею)
- feh (перегляд зображення)
- flameshot (створення скріншотів)
- gnome-software (магазин програм)
- kdeconnect (пов'язання комп'ютера та телефону)

## Інструкція з встановлення


Ставимо Debian. Я поставив Testing. На етапі вибору середовища знімаємо всі галочки. Я залишаю лише ssh сервер, адже Я користуюсь ssh). Після встановлення системи та перезавантаження нас зустрічає чорний екран, який попросить ввести логін. Введіть логін та нажміть Enter. Далі введіть пароль і натисніть Enter. Ви в системі. Поки в ній нема UI. Переходимо в режим суперкористувача. Вводимо su та пароль суперкористувача. Після цього у вас. Ставимо наступні програми. 
```
   apt install xserver-xorg bash-completion sudo mc htop lightdm feh conky picom kitty firefox-esr thunar flameshot
```

### Обов'язково!
Перевірте наявність пакету xserver-xorg:
```
apt list --installet | grep xserver-xorg
```
Без нього після старту UI не буде працювати клавіатура!!! Також для ноутбуків можна поставити такий пакет, як xbacklight. Він дозволить регулювати яскравість екрану.

Перезавантажуємо систему. Завантажуємо з git конфіги і розпаковуємо їх по місцям. Можна просто розпокувати архів в домашню директорію. Ті масі дії можна зробити в терміналі на стадії встановлення пакетів.
```
cd
wget https://gitlab.com/manzhara157/i3bygamerua/-/archive/awesome/i3bygamerua-awesome.zip
unzip i3bygamerua-awesome.zip
cd i3bygamerua-awesome
cp -r .config/ ~/
cp -r .fonts/ ~/
cp -r .local/ ~/
cd ..
rm -r i3bygamerua-awesome
rm i3bygamerua-awesome.zip
```

### (Далі все буде перекладео пізніше)
Якщо у вас нема файлу /usr/share/backgrounds/debianm.jpg то у вас не отобразится картинка робочего стола. С помощью nitrogen можно сменить её на любую, которая вам нравится.


После всего вы получите плюс-минус такую систему, как на скриншоте.


## Способ для "Ленивых"

Якщо ви не хочете заморачуваться самі, а хочете отримати таку ж систему, то пропоную 2 рішення. 

Перший, це завантажити вже готову збірку за посиланням.
 - https://sites.google.com/view/debi3/main

Другий спосіб, це скрипт, який робить те саме, що описано вище, тільки в автоматичному режимі. Завантажуйте скрипт та запускайте на будь якому етапі !!!від мені користувача!!! 

## Чарівне слово SUDO
Для деяких дій знадобиться права суперкористувача. **sudo** в debian можна налаштувати настпним чином.

- Входимо під суперкристувачем командою su.
- Встановлюємо sudo
```
apt install sudo
```
- прописуємо в терминалі команду
```
usermode -aG sudo UserName
```
де UserName - им'я вашого користувача.
- виходимо з суперкористувача
```
exit
```

Щоб завантажити і запустити скрипт, можна виконати ось таку команду:
```
wget https://gitlab.com/manzhara157/i3bygamerua/-/raw/master/install.sh
sudo chmod +x install.sh
sudo ./install.sh
```

## Моя розкладка
Я багато працюю з текстом різними мовами. Украинский і английский. Інколи московська. Чим більше Я працював з клавіатурою, тим більше мені не подобались місця деяких клавіш. Я наткнувся на статю-інструкнцію зі заміни місця клавіш. В linux все виявилось дуже просто. Я зробив свій варіант розкладки. Це юнікодовська українська розкладка з деякими змінами. Знайти можна в теці layout.

![layout](layout/Manzhara_Layout.png)

Чорним показано звичану розкладку, Червоним показано третій рівень. Я перехожу на третій рівень кнопкою Caps Lock.
Вона не замінює стандартні види розкладок. Для встановлення треба скопіювати файл ua з теки layout до теки `/usr/share/X11/xkb/symbols/` з заміною.

Також можна скопіювати файл us, якщо ви теж хочете мати клавіші Vim на третьому рівні.

Щоб розкладка з'явилась у вашому DE відредагуйте файл `/usr/share/X11/xkb/rules/evdev.xml`. В ньому знайдіть налаштуваннц української мови. Це приблизно 5883 рядок. В `<variantList>` додайте варіан як показано нижче. Далі перелогіньтесь та налаштуйте варіан мови, як зазвичай. 
```
...
<variantList>
        <variant>
          <configItem>
            <name>uam</name>
            <description>Ukrainian (Manzhara)</description>
          </configItem>
        </variant>

        ...

```
Для WM можна застосувати команду:

```
setxkbmap -option "grp:toggle,lv3:caps_switch,grp_led:caps" -layout us,ua -variant ",uam"
```
Саме ця команда налаштовує 2 розкладки (стандартну англійську та розкладку Мандара), а також перемикання розкладки на правий альт.


## Дії після закриття кришки

Вбудований менеджер живлення вміє тільки sleep, suspend і do nothing. Для розширення можливостей, за бажанням, можна встановити набір скриптів ```pm-utils```. Якщо наявного функціоналу досить, то просто редагуємо файл **/etc/systemd/logind.conf**. Наприклад через nano.

```
sudo nano /etc/systemd/logind.conf
```

Розкоментуйте рядок, який подано нижче. (Для цього видаліть #)
```
HandleLidSwitch=suspend
```

**Suspend** можна змінити на потрібну вам дію.


Стандартні:
- sleep
- suspend
- ignore


Додані набором скриптів:
- poweroff
- reboot
- halt
- kexec
- hibernate
- hybrid-sleep
- lock

## Регулювання яскравості

Для регулювання яскравості в x11 використовується програма xbacklight. Якщо вона ноутбук не реагує на ці команди, то можливо, потрібно до додати один конфігураціний файл. Спосіб точно працює з інтегрованою графікою intel.

Створіть файл **/etc/X11/xorg.conf.d/10-intel.conf**. В цей вайл скопіюйте наступний текст:

```
Section "Device"
        Identifier      "Card0"
        Driver          "intel"
        Option          "Backlight" "/sys/class/backlight/"
EndSection
```

## Налаштування тачпаду

Після встановлення базовий функціонал тачпаду зводиться до переміщення курсору та натиснення кнопок. Для розширення функціоналу тачпаду створіть (або відрегагуйте, якщо він створений) файл за адресою **/etc/X11/xorg.conf.d/70-touchPad-settings.conf**. Внесіть туди наступні рядки.
```
Section "InputClass"
    Identifier "Touchpads"
    MatchIsTouchpad "on"
    Option "Tapping" "on"
    Option          "MinSpeed"              "0.5"
    Option          "MaxSpeed"              "1.0"
    Option          "AccelFactor"           "0.075"
    Option          "TapButton1"            "1"
    Option          "TapButton2"            "2"     # multitouch
    Option          "TapButton3"            "3"     # multitouch
EndSection
```

Більше параметрів можна подивитись за посиланням в блоці "Сайти"

## Для користувачів Discord

Якщо ви користуєтесь Discord, то для нормальної роботи вам знадобиться пакет ```fonts-noto-color-emoji```. Багато хто використовує емодзі в назвах чатів. Без цього пакету замість емодзу будуть квадратики.
